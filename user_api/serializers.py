from rest_framework import serializers
from .models import Employee ,Project, Project_Modules, Company, Role
from django.contrib.auth.hashers import make_password


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model=Project
        fields="__all__"


class Project_ModulesSerializer(serializers.ModelSerializer):
    class Meta:
        model=Project_Modules
        fields='__all__'


class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model=Role
        fields='__all__'


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model=Company
        fields='__all__'


class EmployeeSerializer(serializers.ModelSerializer):
    """ model Employee serializer  """

    class Meta:
        model=Employee
        fields = '__all__'
        extra_kwargs = {
            'password':{'write_only': True},
        }
        
    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data['password'])
        return super(EmployeeSerializer, self).create(validated_data)


# jab be tom python manager.py shell ko use karo check karne kay leya 
# import serializer or object ko karo
#agr koi object data howa hai to 
#all_data=EmployeeSerializer(Employee.objects.all(),many=True)
#dehan rakna many=true zarur rakna


