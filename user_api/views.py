# from django.shortcuts import render  # ya wala code tab use ho ga jab ap templates use karo html wale

#old method of implimentation of API
from django.http import HttpResponse ,JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser


#regular  import model and serializers 
from .models import Employee, Role, Project, Project_Modules, Company
from .serializers import EmployeeSerializer ,RoleSerializer, ProjectSerializer, Project_ModulesSerializer, CompanySerializer

#new imports for API
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view


#new import for method 3
from django.http import Http404
from rest_framework.views import APIView


#new import for method 4
from rest_framework import mixins
from rest_framework import generics


# auth imports 
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token


#authentication
from rest_framework.authentication import TokenAuthentication


from datetime import datetime
from rest_framework.permissions import IsAuthenticated




############################################   serizlizer for listing/ showing all        ########################################################


class EmployeeSareDeka(generics.ListAPIView):
    queryset=Employee.objects.all()
    serializer_class=EmployeeSerializer
    permission_classes = [IsAuthenticated]

class RoleList(generics.ListAPIView):
    queryset=Role.objects.all()
    serializer_class=RoleSerializer

class CompanyList(generics.ListAPIView):
    queryset=Company.objects.all()
    serializer_class=CompanySerializer


class ProjectList(generics.ListAPIView):
    queryset=Project.objects.all()
    serializer_class=ProjectSerializer


class Project_ModulesList(generics.ListAPIView):
    queryset=Project_Modules.objects.all()
    serializer_class=Project_ModulesSerializer


#########################################################    Serializer for creating                 ###############################################################

class EmployeeCreate(generics.CreateAPIView):
    queryset=Employee.objects.all()
    serializer_class=EmployeeSerializer
    # permission_classes = [IsAuthenticated]

class RoleCreate(generics.CreateAPIView):
    queryset=Role.objects.all()
    serializer_class=RoleSerializer

class CompanyCreate(generics.CreateAPIView):
    queryset=Company.objects.all()
    serializer_class=CompanySerializer


class ProjectCreate(generics.CreateAPIView):
    queryset=Project.objects.all()
    serializer_class=ProjectSerializer


class Project_ModulesCreate(generics.CreateAPIView):
    queryset=Project_Modules.objects.all()
    serializer_class=Project_ModulesSerializer



#########################################################    Serializer for Deleting               ###############################################################

class EmployeeDelete(generics.DestroyAPIView):
    queryset=Employee.objects.all()
    serializer_class=EmployeeSerializer
    # permission_classes = [IsAuthenticated]

class RoleDelete(generics.DestroyAPIView):
    queryset=Role.objects.all()
    serializer_class=RoleSerializer

class CompanyDelete(generics.DestroyAPIView):
    queryset=Company.objects.all()
    serializer_class=CompanySerializer


class ProjectDelete(generics.DestroyAPIView):
    queryset=Project.objects.all()
    serializer_class=ProjectSerializer


class Project_ModulesDelete(generics.DestroyAPIView):
    queryset=Project_Modules.objects.all()
    serializer_class=Project_ModulesSerializer



#########################################################    Serializer for Updating               ###############################################################

class EmployeeUpdate(generics.UpdateAPIView):
    queryset=Employee.objects.all()
    serializer_class=EmployeeSerializer
    # permission_classes = [IsAuthenticated]

class RoleUpdate(generics.UpdateAPIView):
    queryset=Role.objects.all()
    serializer_class=RoleSerializer

class CompanyUpdate(generics.UpdateAPIView):
    queryset=Company.objects.all()
    serializer_class=CompanySerializer


class ProjectUpdate(generics.UpdateAPIView):
    queryset=Project.objects.all()
    serializer_class=ProjectSerializer


class Project_ModulesUpdate(generics.UpdateAPIView):
    queryset=Project_Modules.objects.all()
    serializer_class=Project_ModulesSerializer


#########################################################    Serializer for Retrieve               ###############################################################

class EmployeeRetrieve(generics.RetrieveAPIView):
    queryset=Employee.objects.all()
    serializer_class=EmployeeSerializer
    # permission_classes = [IsAuthenticated]

class RoleRetrieve(generics.RetrieveAPIView):
    queryset=Role.objects.all()
    serializer_class=RoleSerializer

class CompanyRetrieve(generics.RetrieveAPIView):
    queryset=Company.objects.all()
    serializer_class=CompanySerializer


class ProjectRetrieve(generics.RetrieveAPIView):
    queryset=Project.objects.all()
    serializer_class=ProjectSerializer


class Project_ModulesRetrieve(generics.RetrieveAPIView):
    queryset=Project_Modules.objects.all()
    serializer_class=Project_ModulesSerializer





















class loginEmployee(APIView):
    def post(self, request):
        # Employees=Employee.objects.all()
        # request_Employee=request.data
        # for i in Employees:
        #     if request_Employee['name']==i.name:
        #         print("find the name")
        #         if request_Employee['password']==i.password:
        #             return Response("yes you are login")
        # return Response(f" Sorry you got wrong information " )



        # r_data=request.data 
        # try:
        #     first_check=Employee
        # .objects.get(email=r_data['email'])
        #     if first_check.password==r_data['password']:
        #         return Response(f'welcome: {first_check.name}',status=status.HTTP_202_ACCEPTED)
        #     return Response(f"sorry  {r_data['name']} your password is not correct")
        # except:
        #     return Response("we dont know you sorry",status=status.HTTP_404_NOT_FOUND)


        serializer =LoginSerializer(data=request.data)
        if serializer.is_valid():

            print(str(serializer.data.get('email')))
            
            print(serializer.data.get('password'))
            # if first_check.password==serializer.data.get('password'):
            #     return Response(f'welcome: {first_check.name}',status=status.HTTP_202_ACCEPTED)

            try:
                first_check=Employee.objects.get(email=serializer.data.get('email'))
                if first_check.password==serializer.data.get('password'):
                    return Response("yes yee")
            except:
                return Response("sorry")
            return Response(serializer.data)

        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
            




    