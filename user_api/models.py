from django.db import models
from .EnumConstant import Gender 
# Current date time in local system



class Company(models.Model):
    """ this model will save all the company name. which give project to Metis """

    company_name=models.CharField(max_length=20,unique=True)
    loaction=models.CharField(max_length=50)

    def __str__(self):
        return self.company_name


class Role(models.Model):
    """ Role for the Employee """

    role_name=models.CharField(max_length=20)
    is_role_active=models.BooleanField(default=True)


    def __str__(self):
        return self.role_name
    

class Employee(models.Model):
    """ model for Employee """


    first_name=models.CharField(max_length=100)
    last_name=models.CharField(max_length=100)
    gender=models.CharField(max_length=1,choices=[(tag, tag.value) for tag in Gender])
    language=models.CharField(max_length=20)
    data_of_joining=models.DateField(auto_now_add=True)
    is_active=models.BooleanField(default=True)
    email=models.EmailField(unique=True) # email must be unique because we are going to use in login
    phone = models.PositiveBigIntegerField(blank=True)
    password=models.CharField(max_length=100)
    # profile_img=models.ImageField()
    role=models.OneToOneField(
        Role,on_delete=models.CASCADE)
    experience=models.PositiveIntegerField(default=0)

    

    

    def __str__(self):
        return self.last_name
    

class Project(models.Model):
    """ This model is responsable for storing the project details"""

    project_name=models.CharField(max_length=50)
    project_starting_time=models.DateTimeField(auto_now_add=True)
    project_submitting_data=models.DateField(null=True, blank=True)
    company_name=models.ForeignKey(Company, max_length=20, on_delete=models.DO_NOTHING, null=True, blank=True)
    # DO_NOTHING mean you when the company is deleted you dont have to delete the porject
    #null and blank true is because if you dont the company name you can add is some other time



    def __str__(self):
        return self.project_name
    




class Project_Modules(models.Model):
    """ This model divide the project into chunks so that modules are divide into Employee """

    project_name=models.ForeignKey(Project, on_delete=models.CASCADE )
    #CASCADE because when the project is deleted you dont need the  modules of the 
    employee_name=models.ForeignKey(Employee, on_delete=models.CASCADE )
    module_name=models.CharField(max_length=50)
    description=models.TextField()
    assigning_data=models.DateTimeField(auto_now_add=True)
    finishing_data=models.DateField(null=True, blank=True)
    completed=models.BooleanField(default=False)

    if completed:
        finishing_data=models.DateField(auto_now=True)

    def __str__(self):
        return self.module_name + " : "
    









